﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;

namespace CMDoku
{
    static class Program
    {
        private static Suduko Puzzle { get; set; }

        private static ConsoleKey[] AllowedCommands => new[]
        {
            ConsoleKey.Insert,
            ConsoleKey.Delete,
            ConsoleKey.Escape,
            ConsoleKey.Home,
            ConsoleKey.End
        };

        private static char[] XAllowedInput => new[]
        {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'
        };

        private static char[] XAllowedInputUpper => new[]
        {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'
        };

        private static char[] YAllowedInput => new[]
        {
            '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };

        private static char[] NumericAllowedInput => new[]
        {
            '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };

        public static Input LastInput { get; set; }

        private static string[] MenuLinesMain => new[]
        {
            "       _____ __  __ _____ CMD    _  By",
            "      / ____|  \\/  |  __ \\Sudoku| | Mark V.",
            "     | |    | \\  / | |  | | ___ | | ___   _",
            "     | |    | |\\/| | |  | |/ _ \\| |/ / | | |",
            "     | |____| |  | | |__| | (_) |   <| |_| |",
            "      \\_____|_|  |_|_____/ \\___/|_|\\_\\\\__,_|",
            "     ",
            "     Prompts",
            "     ________________________________________",
            "     $: │ See Commands below",
            "     X: │ X axis reference (A-I)",
            "     Y: │ Y axis reference (1-9)",
            "     #: │ Value of the cell (1-9)",
            "     Commands",
            "     ________________________________________",
            "     INS:  │ Insert a #Number into X and Y",
            "     DEL:  │ Delete a number at X and Y",
            "     END:  │ Quick hide",
            "     HOME: │ Check puzzle",
            "     ESC:  │ Exit",
            "",
            "",
        };

        private static string[] MenuLinesIntro => new[]
        {
            "       _____ __  __ _____ CMD    _  By",
            "      / ____|  \\/  |  __ \\Sudoku| | Mark V.",
            "     | |    | \\  / | |  | | ___ | | ___   _",
            "     | |    | |\\/| | |  | |/ _ \\| |/ / | | |",
            "     | |____| |  | | |__| | (_) |   <| |_| |",
            "      \\_____|_|  |_|_____/ \\___/|_|\\_\\\\__,_|",
            "",
            "     Pick a puzzle from the menu of the left and",
            "     enter the full name of the puzzle to start.",
            "",
            "     Difficulty increases the higher number puzzle",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        };

        private static string Message { get; set; }

        private static bool QuickHide { get; set; }

        private static string DirectoryPath { get; set; }
        private static string PuzzleFolder { get; set; }
        private static string PuzzleZipPath { get; set; }
        private static List<string> PuzzleNames { get; set; }

        static void Main(string[] args)
        {
            Console.Title = "CMDoku - A cmd line sudoku game by Mark V.";

            DirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CMDoku";
            PuzzleFolder = DirectoryPath + "\\puzzles";
            PuzzleZipPath = DirectoryPath + "\\puzzles.zip";

            if (args != null && args.Length > 0 && args.First().ToLower() == "init")
            {
                bool hard = args.Length > 1 && args[1].ToLower() == "--hard";

                Init(hard);
            }
            else if (!Directory.Exists(DirectoryPath) || !Directory.Exists(PuzzleFolder) || !Directory.GetFiles(PuzzleFolder).Any())
            {
                Console.WriteLine("Puzzle folder and files not found");

                if (ConfirmChoice("Do you want to download and install the puzzles?"))
                {
                    Init(false);

                    Console.WriteLine("Press any key to start");
                    Console.ReadKey();
                }
                else
                {
                    Environment.Exit(0);
                }
            }

            var puzzleName = SelectPuzzle();

            Puzzle = LoadPuzzle(puzzleName);

            do
            {
                Console.Clear();
                
                if (!QuickHide)
                {
                    RenderGrid(Puzzle);
                }
                else
                {
                    Console.WriteLine("Marcrosoft Mindows [Version 10.0.420.69]\r\n" +
                                      "(c) 2019 Marcrosoft Corporation. No rights reserved.");
                }

                if (!string.IsNullOrEmpty(Message))
                {
                    Console.WriteLine();
                    Console.WriteLine(Message);
                    Message = null;
                }

                var input = GetInput();

                if (input == null)
                {
                    continue;
                }

                LastInput = input;

                var number = GetNumber(Puzzle, input.X, input.Y);

                if (number.Editable)
                {
                    Puzzle = UpdatePuzzle(Puzzle, input);
                }
                else
                {
                    LastInput = null;
                    Message = $"{XAllowedInputUpper[input.X]},{input.Y + 1} is not editable";
                    continue;
                }

            } while (true);
        }

        public static void Init(bool hard)
        {
            if (hard && Directory.Exists(PuzzleFolder))
            {
                var files = Directory.GetFiles(PuzzleFolder);

                foreach (var file in files)
                {
                    File.Delete(file);
                }

                Directory.Delete(PuzzleFolder);
            }

            Directory.CreateDirectory(DirectoryPath);
            Directory.CreateDirectory(PuzzleFolder);

            try
            {
                File.Delete(PuzzleZipPath);

                using (var client = new WebClient())
                {
                    client.DownloadFile("https://gitlab.com/markvardy/cmdoku/raw/master/sudokus.zip", PuzzleZipPath);
                }

                ZipFile.ExtractToDirectory(PuzzleZipPath, PuzzleFolder);

                Console.WriteLine("Puzzle files succesfully downloaded");
            }
            catch (IOException e)
            {
                Console.WriteLine("The puzzle files may already exists. Use \"init --hard\" to delete and try again");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error downloading puzzles");
            }
        }

        public static string SelectPuzzle()
        {
            var files = Directory.GetFiles(PuzzleFolder);

            PuzzleNames = new List<string>();

            var numberedPuzzles = new List<int>();
            var otherPuzzles = new List<string>();

            foreach (var file in files)
            {
                var puzzleName = file.Split('\\').Last().Split('.').First();

                if (int.TryParse(puzzleName, out var puzzleId))
                {
                    numberedPuzzles.Add(puzzleId);
                }
                else
                {
                    otherPuzzles.Add(puzzleName);
                }
            }

            numberedPuzzles.Sort();

            PuzzleNames = numberedPuzzles.Select(x => x.ToString()).Concat(otherPuzzles).ToList();

            do
            {
                Console.Clear();

                DrawPuzzleMenu();
                
                Console.Write(">");

                var input = Console.ReadLine();

                if (!PuzzleNames.Contains(input))
                {
                    continue;
                }

                return input;

            } while (true);
        }

        public static void DrawPuzzleMenu()
        {
            Console.WriteLine();
            Console.WriteLine("  ┌────┬────┬────┬────┬────┐  " + MenuLinesIntro[0]);
            Console.Write("  │ ");

            var menuCounter = 1;

            int colCounter = 1;

            foreach (var puzzleName in PuzzleNames)
            {
                var fileName = puzzleName;

                if (fileName.Length < 2)
                {
                    fileName += " ";
                }

                Console.Write(fileName + " │ ");

                if (colCounter % 5 == 0)
                {
                    Console.WriteLine(" " + MenuLinesIntro[menuCounter]);
                    menuCounter++;
                    Console.WriteLine("  ├────┼────┼────┼────┼────┤  " + MenuLinesIntro[menuCounter]);
                    Console.Write("  │ ");

                    menuCounter++;
                }

                colCounter++;
            }

            var remainder = 5 - PuzzleNames.Count % 5;

            for (int i = 0; i < remainder; i++)
            {
                Console.Write("-- │ ");
            }

            Console.WriteLine();
            Console.WriteLine("  └────┴────┴────┴────┴────┘");
            Console.WriteLine();
        }
        
        public static Suduko LoadPuzzle(string puzzleName)
        {
            var puzzle = new Suduko();

            using (StreamReader fs = new StreamReader($"{PuzzleFolder}\\{puzzleName}.txt"))
            {
                for (int y = 0; y < 9; y++)
                {
                    var line = fs.ReadLine()?.Split(' ').Where(x => !string.IsNullOrEmpty(x)).Select(int.Parse).ToArray();

                    for (int x = 0; x < 9; x++)
                    {
                        //If it errors. It errors. 🥊
                        var number = line[x];

                        puzzle.Grid[x, y] = new SudukoNumber
                        {
                            Value = number,
                            Editable = number == 0 //0 indicates empty space
                        };
                    }
                }
            }

            return puzzle;
        }
        
        public static bool CheckPuzzle(Suduko puzzle)
        {

            for (int y = 0; y < 9; y++)
            {
                List<int> row = new List<int>();

                for (int x = 0; x < 9; x++)
                {
                    row.Add(puzzle.Grid[x, y].Value);
                }

                if (row.Contains(0) || row.Sum() != 45 || row.Distinct().Count() != 9)
                {
                    return false;
                }
            }

            for (int x = 0; x < 9; x++)
            {
                List<int> col = new List<int>();

                for (int y = 0; y < 9; y++)
                {
                    col.Add(puzzle.Grid[x, y].Value);
                }

                if (col.Contains(0) || col.Sum() != 45 || col.Distinct().Count() != 9)
                {
                    return false;
                }
            }

            //Loop through the 9 different 3x3 blocks
            for (int blockX = 0; blockX < 9; blockX = blockX + 3)
            {
                for (int blockY = 0; blockY < 9; blockY = blockY + 3)
                {
                    List<int> block = new List<int>();

                    for (int x = 0; x < 3; x++)
                    {
                        for (int y = 0; y < 3; y++)
                        {
                            block.Add(puzzle.Grid[blockX + x, blockY + y].Value);
                        }
                    }

                    if (block.Contains(0) || block.Sum() != 45 || block.Distinct().Count() != 9)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        
        public static SudukoNumber GetNumber(Suduko puzzle, int x, int y)
        {
            return puzzle.Grid[x, y];
        }

        public static Suduko UpdatePuzzle(Suduko puzzle, Input input)
        {
            switch (input.Type)
            {
                case Command.Insert:
                {
                    var number = puzzle.Grid[input.X, input.Y];

                    number.Value = input.Number;

                    puzzle.Grid[input.X, input.Y] = number;

                    Message = $"Setting {XAllowedInputUpper[input.X]},{input.Y + 1} to {input.Number}";

                    break;
                }
                case Command.Delete:
                {
                    var number = puzzle.Grid[input.X, input.Y];

                    number.Value = input.Number;

                    puzzle.Grid[input.X, input.Y] = number;

                    Message = $"Deleting {XAllowedInputUpper[input.X]},{input.Y + 1}";

                    break;
                }
            }

            return puzzle;
        }
        
        public static Input GetInput()
        {
            Console.Write("\r\n$:");
            var commandRaw = Console.ReadKey();

            if (AllowedCommands.Contains(commandRaw.Key))
            {
                switch (commandRaw.Key)
                {
                    case ConsoleKey.Insert:
                    {
                        Console.Write("INS");

                        return GetCoordinatesInput(Command.Insert);
                    }
                    case ConsoleKey.Delete:
                    {
                        Console.Write("DEL");

                        return GetCoordinatesInput(Command.Delete);
                    }
                    case ConsoleKey.Home:
                    {
                        Console.Write("HOME");

                        Message = CheckPuzzle(Puzzle) ? "Puzzle has been completed!" : "Puzzle is not complete";

                        return null;
                    }
                    case ConsoleKey.End:
                    {
                        QuickHide = !QuickHide;

                        Console.Title = QuickHide ? "C:\\WINDOWS\\system32\\cmd.exe" : "CMDoku - A cmd line sudoku game by Mark V.";

                        return null;
                    }
                    case ConsoleKey.Escape:
                    {
                        Console.Clear();

                        if (ConfirmChoice("Are you sure you want to exit?"))
                        {
                            Environment.Exit(0);
                        }
                        
                        return null;
                    }
                }
            }

            Message = "Please enter a valid command key";

            return null;
        }

        public static bool ConfirmChoice(string message)
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write(message + " ");

                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.Write("Y");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write("/");
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.Write("N");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write(": ");

                var tryAgainInput = Console.ReadLine();

                if (tryAgainInput != null)
                {
                    if (tryAgainInput.ToLower() == "y")
                    {
                        return true;
                    }
                    else if (tryAgainInput.ToLower() == "n")
                    {
                        return false;
                    }
                }
            }
        }
        
        public static Input GetCoordinatesInput(Command command)
        {
            Console.Write("\r\nX: ");
            var xInputRaw = Console.ReadKey();
            char xInput;

            if (XAllowedInput.Contains(xInputRaw.KeyChar))
            {
                xInput = xInputRaw.KeyChar;
            }
            else if (XAllowedInputUpper.Contains(xInputRaw.KeyChar))
            {
                //Convert upper to lower
                xInput = XAllowedInput[Array.IndexOf(XAllowedInputUpper, xInputRaw.KeyChar)];
            }
            else
            {
                Message = "Please enter A-I for the X axis";

                return null;
            }

            Console.Write("\r\nY: ");
            var yInputRaw = Console.ReadKey();
            char yInput;

            if (YAllowedInput.Contains(yInputRaw.KeyChar))
            {
                yInput = yInputRaw.KeyChar;
            }
            else
            {
                Message = "Please enter 1-9 for the Y axis";

                return null;
            }

            int numberInput = 0;

            if (command == Command.Insert)
            {
                Console.Write("\r\n#: ");
                var numberInputRaw = Console.ReadKey();

                if (NumericAllowedInput.Contains(numberInputRaw.KeyChar))
                {
                    numberInput = int.Parse(numberInputRaw.KeyChar.ToString());
                }
                else
                {
                    Message = "Please enter 1-9 for the Input";

                    return null;
                }
            }

            Console.WriteLine();

            int x = Array.IndexOf(XAllowedInput, xInput);
            int y = Array.IndexOf(YAllowedInput, yInput);

            return new Input
            {
                X = x,
                Y = y,
                Number = numberInput,
                Type = command
            };
        }
        
        public static void RenderGrid(Suduko puzzle)
        {
            Console.ForegroundColor = ConsoleColor.White;

            int col = 0;
            int row = 0;
            int menuCounter = 2;//Start at 2 cos the first 2 lines of the grid are static

            Console.WriteLine("     A   B   C   D   E   F   G   H   I   " + MenuLinesMain[0]);
            Console.WriteLine("   ╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗ " + MenuLinesMain[1]);

            for (int y = 0; y < 9; y++)
            {
                Console.Write($" {y+1} ║ ");

                for (int x = 0; x < 9; x++)
                {
                    var number = puzzle.Grid[x, y];

                    if (LastInput != null && LastInput.X == x && LastInput.Y == y)
                    {
                        if (LastInput.Type == Command.Insert)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                        }
                        else if (LastInput.Type == Command.Delete)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        LastInput = null;
                    }
                    else if (!number.Editable)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }

                    var colSeparator = "│";

                    if (col == 2 || col == 5 || col == 8)
                    {
                        colSeparator = "║";
                    }

                    if (number.Value == 0)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write(number.Value);
                    }

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    Console.Write($" {colSeparator} ");

                    col++;
                }

                col = 0;

                row++;

                Console.Write(MenuLinesMain[menuCounter]);
                menuCounter++;
                Console.WriteLine();

                if (row == 9)
                {
                    Console.WriteLine("   ╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝ " + MenuLinesMain[menuCounter]);
                }
                else if(row == 3 || row == 6)
                {
                    Console.WriteLine("   ╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣ " + MenuLinesMain[menuCounter]);
                }
                else
                {
                    Console.WriteLine("   ╠───┼───┼───╬───┼───┼───╬───┼───┼───╣ " + MenuLinesMain[menuCounter]);
                }

                menuCounter++;
            }
        }
    }

    public enum Command
    {
        Insert,
        Delete,
        Save,
        Reset
    }

    public class Input
    {
        public Command Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Number { get; set; }
    }

    public class Suduko
    {
        public Suduko()
        {
            Grid = new SudukoNumber[9,9];
        }

        public SudukoNumber[,] Grid { get; set; }
    }
    
    public class SudukoNumber
    {
        public SudukoNumber()
        {

        }

        public int Value { get; set; }
        public bool Editable { get; set; }
        public bool Correct { get; set; }
    }
}
