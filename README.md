# CMDoku

A command line Sudoku game for the windows console.

![cmdoku](screenshot.png)

Download the latest version [here](https://gitlab.com/markvardy/cmdoku/raw/master/CMDoku.zip?inline=false)